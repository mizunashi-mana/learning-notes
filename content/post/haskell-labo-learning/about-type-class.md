---
date: 2017-06-15T04:24:15+09:00
categories:
- haskell-labo-learning
title: 型クラスについて
tags:
- haskell
---

レジュメ。

## 構成

1. 型クラスの基本事項
    1. 型クラスとはどういうものか
    1. 型クラスの使い方の紹介
    1. Goal: 型クラスが使えるようになる
1. 型クラスの型関数としての見方
    1. ConstraintKinds拡張について
    1. 型クラスの型関数としての見方について
    1. constraintsパッケージの紹介
    1. Goal: constraintsパッケージの内容が理解できるようになる
1. 型クラスのGHCでの実装の話
    1. 型クラスの暗黙的引数としての見方について
    1. 型クラスの消去について
    1. reflectionパッケージの紹介
    1. Goal: reflectionパッケージの内容が理解できるようになる

<!--more-->

## 型クラスの基本事項

### 型クラスの基本的な用語の確認

* 型クラス(type class): 型のクラス(類)
* メソッド(method): 型クラスに付随する関数

### 性質と証明としての見方

* 型クラス定義: 型に対する性質の定義
* 型クラスインスタンス定義: 性質の証明

Semigroupの例

* Haskellは証明支援が弱いので、足りない部分は手動で補う
    - できる証明は型があってるかの確認ぐらい
* 型クラスの性質は実際には型とメソッドの組$(T, m\_0, \ldots , m\_n)$に対して定義されることに注意
    - これを濫用して、型に関する性質と呼んでいる
* 性質は、最適化などに使用される場合があるので注意
    - 例: FoldableのfoldMapはMonoidの性質があることが前提

### 型クラスに付随する用語の確認 

* 継承(class extension/inheritance): スーパークラスの制約のもと型クラスを作る

* パラメータ多相(parametric polymorphism)
    - パラメータに対して、具体的な値を考慮しないような多相性
    - パラメータを具体的な値に、単純に置換することで、目的の操作が得られる
* アドホック多相(ad hoc polymorphism)
    - パラメータのそれぞれの値に対して、振る舞いが定義されるような多相性
    - 振る舞いを決定するプロセスは、実装によって異なる

* 種(kind): 型の型
    - 型クラスでは、種シグネチャを省略すると、勝手に推論される
    - `*`: inhabited kind

## 型クラスの型関数としての見方

### 型関数としての見方

```haskell
>>> type A a = Maybe a
>>> :k A
A :: * -> *
```

* 型関数: 型を受け取って型を返す関数

* 高階型関数: 型関数を受け取って型関数を返す関数

### ConstraintKinds拡張について

* 種(kind): 型の型

* 型クラスの種: `k -> Constraint`
    - `Semigroup`: `* -> Constraint`
    - `Functor`: `(* -> *) -> Constraint`

* 制約種(constraint kind): アドホック多相の振る舞いが定義されているかの制約を表す
    - 制約を満たしている**ならば(=>)**こういう型

* 型クラスエイリアス: `type synonym`の制約種への拡張

```haskell
>>> :set -XConstraintKinds
>>> type MonMonad m a = (Monad m, Monoid (m a))
>>> :k MonMonad
MonMonad :: (* -> *) -> * -> Constraint
```

### 制約を受け取る型関数

* 目的: `Constraint -> *`という種を持つ型関数の実現
    - Haskellの仕様上、`=>`は型関数ではない

* アイデア: GADTsによって、データコンストラクタの型に制約を埋め込む

```haskell
>>> :set -XGADTs
>>> data Dict (a :: Constraint) where Dict :: a => Dict a
>>> :k Dict
Dict :: Constraint -> *
```

`Dict`は制約を受け取って、データ型を作る

```haskell
>>> :t Dict :: Dict (Monoid Ordering)
Dict :: Dict (Monoid Ordering) :: Dict (Monoid Ordering)
>>> :t Dict :: Dict (Monoid Bool)

<interactive>:1:1: error:
    • No instance for (Monoid Bool) arising from a use of ‘Dict’
    • In the expression: Dict :: Dict (Monoid Bool)
>>> type MonoidDict m = Dict (Monoid m)
>>> :k MonoidDict
MonoidDict :: * -> *
>>> :t Dict :: MonoidDict Ordering
Dict :: MonoidDict Ordering :: MonoidDict Ordering
>>> :t Dict :: MonoidDict Bool

<interactive>:1:1: error:
    • No instance for (Monoid Bool) arising from a use of ‘Dict’
    • In the expression: Dict :: MonoidDict Bool
```

* Constraintは種に現れていないのに、制約を書けることができる
    - Constraintをデータとしてエンコーディング

### 二階述語論理で制約を扱う

* 一階述語

```haskell
(<$>) :: Functor f => (a -> b) -> f a -> f b
(<$>) = fmap
```

* 制約の適用
    - Dictデータ型のGADTsを紐解き、Rank2のConstraintに適用

```haskell
(&) :: a -> (a -> b) -> b
x & f = f x

withDict :: Dict a -> (a => r) -> r
withDict d r = case d of Dict -> r
```

### 制約の圏

[圏論の基本事項](/post/haskell-labo-learning/intro-category-theory/)も参照

* アイデア: 制約の`:-`(entailment/必要条件)を射とする
    - 部分制約(`a => Dict b`): `a => Dict b`$\land$`b => r`$\Rightarrow$`a => r`
    - 部分制約は、制約から制約への射

```haskell
newtype a :- b = Sub (a => Dict b)
```

* 圏の恒等射と合成
    - refl: reflexive(反射)
    - trans: transitive(推移)

* 制約の圏はデカルト閉圏
    - terminal object(終対象): `()` ($\forall a \in Constraints\ldotp a :- ()$)
    - product(積): `(a, b)` ($\forall a, b, c \in Constraints\ldotp a :- b \land a :- c \Rightarrow a :- (b, c)$)
    - exponential object(冪対象): `a :- (b, c)` ($\forall a, b, c \in Constraints\ldotp Dict b \land a :- c \Rightarrow a :- (b, c)$)
        - ここでの$(a\_1, \cdots, a\_n)$は単なるタプルではなく、制約の表記法であることに注意
    
    - weaken(弱化): 制約を弱める。積の射影に相当(AかつBの制約の下ではAの制約は満たされる)
    - strengthen(強化): 制約の導出をより強くする。冪のliftingに相当(Aの制約からCの制約が導出できるなら、Aの制約からCかつBの制約の導出が作れる)

* 充満忠実(fully faithful)関手
    - 制約の圏(射は`:-`)と$Hask$圏の部分圏$Hask_{Dict}$($Dict: Constraints \to Hask$関手で作られた圏)は一対一対応
    - `a :- b`$\Leftrightarrow$`Dict a -> Dict b`

* 始対象(initial object)と終対象(terminal object)
    - initial object(始対象): `Bottom`($\forall P, Q\ldotp (P \land \neg P) \Rightarrow Q$)

        ```haskell
        >>> import GHC.Exts
        >>> :t undefined :: Any => Int

        <interactive>:1:1: error:
            Could not deduce: Any arising from a use of ‘it’
        ```

    - terminal object(終対象): `()`(再掲)

### Reflection

実行時に型情報を取る(実行時の型情報を保証(reify)する)

* 型クラスの継承関係の保証(型クラスレベルで情報を落とす)

    ```haskell
    class Class b h | h -> b where
        cls :: h :- b
    
    class Foo a where
        foo :: a -> a

    instance Class () (Foo a) where
        cls = Sub Dict :: Foo a :- ()
    
    class Foo a => Bar a where
        bar :: a -> a
    
    instance Class (Foo a) (Bar a) where
        cls = Sub Dict :: Bar a :- Foo a
    ```

    - Haskellの継承関係のシンタックスは、条件が逆転していることに注意
        - `class Foo a => Bar a`は、`Bar a`ならば`Foo a`が成り立つの意
        - `Bar a => r`において、`Foo a`のインスタンスが**reify**できる
        - PureScriptではちゃんとこれを考慮し、`class Foo a <= Bar a`というシンタックスを採用している

* インスタンスの継承関係の保証(型レベルで情報を落とす)

    ```haskell
    class b :=> h | h -> b where
        ins :: b :- h
    
    class Foo a where
        foo :: a -> a
    
    instance Foo Bool where
        foo = not
    
    instance () :=> Foo Bool where
        ins = Sub Dict :: () :- Foo Bool

    instance Foo a => Foo [a] where
        foo = fmap foo
    
    instance Foo a :=> Foo [a] where
        ins = Sub Dict :: Foo a :- Foo [a]
    ```

* これらを使って、インスタンスの導出が可能(手動の導出証明が可能)
    - Constraints Programming Tools: `Dict`/`withDict`/`:-`/`\\`/`Category`/Constraints `Arrow`/CCC/`cls`/`ins`

### constraintsパッケージの有用モジュール

* `Data.Constraint.Deferable`: 実行時型制約チェック
* `Data.Constraint.Forall`: `(forall a. f a) =>`を書くための、`Forall f`を提供
* `Data.Constraint.Lifting`: `(forall a. p a => p (f a)) =>`を書くための、`Lifting p f`を提供

## 型クラスのGHCでの実装

### 辞書渡し

```haskell
class AClass a where
  aMethod1 :: a -> a
  aMethod2 :: a -> a

instance AClass Bool where
  aMethod1 = not
  aMethod2 = id

useAClass :: AClass a => a -> a
useAClass = aMethod1 . aMethod2
{-# NOINLINE useAClass #-}

boolUseAClass :: Bool -> Bool
boolUseAClass = useAClass
```

* クラス定義: 辞書データ構造の定義
* インスタンス定義: 辞書オブジェクトの定義

```haskell
data Dict__AClass a = Dict__AClass
  { aMethod1 :: a -> a
  , aMethod2 :: a -> a
  }

dict__AClass_Bool :: Dict__AClass Bool
dict__AClass_Bool = Dict__AClass
  { aMethod1 = not
  , aMethod2 = id
  }

useAClass :: Dict_AClass a -> a -> a
useAClass d = aMethod1 d . aMethod2 d

boolUseAClass :: Bool -> Bool
boolUseAClass = useAClass dict__AClass_Bool
```

```
$ stack ghc -- src/TestTypeClass.hs -O0 -ddump-simpl -fforce-recomp -dsuppress-var-kinds
[1 of 1] Compiling TestTypeClass    ( src/TestTypeClass.hs, src/TestTypeClass.o )

==================== Tidy Core ====================
Result size of Tidy Core = {terms: 57, types: 61, coercions: 0}

-- RHS size: {terms: 6, types: 10, coercions: 0}
aMethod1
  :: forall a_ap2[sk]. AClass a_ap2[sk] => a_ap2[sk] -> a_ap2[sk]
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType <S(SL),U(U,A)>,
 RULES: Built in rule for aMethod1: "Class op aMethod1"]
aMethod1 =
  \ (@ a_ap2[sk]) (tpl_B1 :: AClass a_ap2[sk]) ->
    case tpl_B1 of tpl_B1 { TestTypeClass.C:AClass tpl_B2 tpl_B3 ->
    tpl_B2
    }

-- RHS size: {terms: 6, types: 10, coercions: 0}
aMethod2
  :: forall a_ap2[sk]. AClass a_ap2[sk] => a_ap2[sk] -> a_ap2[sk]
[GblId[ClassOp],
 Arity=1,
 Caf=NoCafRefs,
 Str=DmdType <S(LS),U(A,U)>,
 RULES: Built in rule for aMethod2: "Class op aMethod2"]
aMethod2 =
  \ (@ a_ap2[sk]) (tpl_B1 :: AClass a_ap2[sk]) ->
    case tpl_B1 of tpl_B1 { TestTypeClass.C:AClass tpl_B2 tpl_B3 ->
    tpl_B3
    }

-- RHS size: {terms: 3, types: 2, coercions: 0}
TestTypeClass.$fAClassBool [InlPrag=[ALWAYS] CONLIKE]
  :: AClass Bool
[GblId[DFunId], Str=DmdType]
TestTypeClass.$fAClassBool =
  TestTypeClass.C:AClass @ Bool not (id @ Bool)

-- RHS size: {terms: 7, types: 9, coercions: 0}
useAClass [InlPrag=NOINLINE]
  :: forall a_awa. AClass a_awa => a_awa -> a_awa
[GblId, Arity=1, Str=DmdType]
useAClass =
  \ (@ a_a1s7) ($dAClass_a1s8 :: AClass a_a1s7) ->
    . @ a_a1s7
      @ a_a1s7
      @ a_a1s7
      (aMethod1 @ a_a1s7 $dAClass_a1s8)
      (aMethod2 @ a_a1s7 $dAClass_a1s8)

-- RHS size: {terms: 2, types: 0, coercions: 0}
$trModule1_r1sM :: GHC.Types.TrName
[GblId, Caf=NoCafRefs, Str=DmdType]
$trModule1_r1sM = GHC.Types.TrNameS "main"#

-- RHS size: {terms: 2, types: 0, coercions: 0}
$trModule2_r1t0 :: GHC.Types.TrName
[GblId, Caf=NoCafRefs, Str=DmdType]
$trModule2_r1t0 = GHC.Types.TrNameS "TestTypeClass"#

-- RHS size: {terms: 3, types: 0, coercions: 0}
TestTypeClass.$trModule :: GHC.Types.Module
[GblId, Caf=NoCafRefs, Str=DmdType]
TestTypeClass.$trModule =
  GHC.Types.Module $trModule1_r1sM $trModule2_r1t0

-- RHS size: {terms: 2, types: 0, coercions: 0}
$tc'C:AClass1_r1t1 :: GHC.Types.TrName
[GblId, Caf=NoCafRefs, Str=DmdType]
$tc'C:AClass1_r1t1 = GHC.Types.TrNameS "'C:AClass"#

-- RHS size: {terms: 5, types: 0, coercions: 0}
TestTypeClass.$tc'C:AClass :: GHC.Types.TyCon
[GblId, Caf=NoCafRefs, Str=DmdType]
TestTypeClass.$tc'C:AClass =
  GHC.Types.TyCon
    13765354689115941991##
    3204938150148513756##
    TestTypeClass.$trModule
    $tc'C:AClass1_r1t1

-- RHS size: {terms: 2, types: 0, coercions: 0}
$tcAClass1_r1t2 :: GHC.Types.TrName
[GblId, Caf=NoCafRefs, Str=DmdType]
$tcAClass1_r1t2 = GHC.Types.TrNameS "AClass"#

-- RHS size: {terms: 5, types: 0, coercions: 0}
TestTypeClass.$tcAClass :: GHC.Types.TyCon
[GblId, Caf=NoCafRefs, Str=DmdType]
TestTypeClass.$tcAClass =
  GHC.Types.TyCon
    6656679332973910366##
    1515592465081456118##
    TestTypeClass.$trModule
    $tcAClass1_r1t2

-- RHS size: {terms: 2, types: 1, coercions: 0}
boolUseAClass :: Bool -> Bool
[GblId, Str=DmdType]
boolUseAClass = useAClass @ Bool TestTypeClass.$fAClassBool
```

* Haskellはインライン展開レベルが高いので、展開される前のものを知る時はNOINLINEプラグマを使うといい

### 型クラスの暗黙的引数としての見方

```haskell
unitShow :: String
unitShow = show () -- (dictm__show dict__show_unit) ()
```

* 辞書データを暗黙的に渡している
    - なお、グローバル検索になるので、暗黙的引数として実際に使いたい場合`ImplicitParams`拡張を使う

### 辞書のnewtype

一つしかクラスメソッドの無いクラスの辞書

```haskell
data AClassDict a = AClassDict
  { aClassMethod :: a -> a
  }
```

`newtype`にできるのでは？

```haskell
newtype AClassDict a = AClassDict
  { aClassMethod :: a -> a
  }
```

この場合、`Coerciable (AClassDict a) (a -> a)`になる。実際には型クラスの実装は仕様には無いので、辞書渡しで無い場合はこのような見方はできないが、GHCに限ってこのような見方が可能

辞書は`Coercible`が実際には提供されないので(内部的には`Coercible`になっており、コア言語レベルでは利用されている)、`unsafeCoerce`を使うことで、埋め込みが可能

### reflectionパッケージ

* 基本的なアイデア:
    - メソッド一つの辞書はnewtypeにより実行時は元の値がそのまま使用される
    - メソッド一つの型クラスに対し、`a => r`を`a -> r`に`unsafeCoerce`し、メソッドの型と同じ型の値を適用すれば、そのまま暗黙的引数として使える

* 型推論がいい感じに効くバージョン

```haskell
class Reifies s a | s -> a where
    reflect :: Proxy s -> a

instance Reifies () Int where
    reflect _ = 0

newtype Magic a r = Magic (forall (s :: *). Reifies s a => Proxy s -> r)

reify :: forall a r. a -> (forall (s :: *). Reifies s a => Proxy s -> r) -> r
reify a k = unsafeCoerce (Magic k :: Magic a r) (const a) Proxy

useReflect :: Reifies s Int => Proxy s -> String
useReflect p = show $ reflect p

useReify :: Int -> String
useReify i = reify i useReflect
```

* 型を明示するバージョン(その代わり、インスタンスはなくていい)

```haskell
class Given a where
    given :: a

newtype Give a r = Give (Given a => r)

give :: forall a r. a -> (Given a => r) -> r
give a k = unsafeCoerce (Give k :: Give a r) a

useGiven :: (Given Int, Given String) => String
useGiven = show (given @ Int) <> (given @ String)

useGive :: Int -> String -> String
useGive i s = give i $ give s useGiven
```

## Links

* [Haskell.orgのチュートリアル - Type Classes](https://www.haskell.org/tutorial/classes.html)
* [Haskell Wiki - Kind](https://wiki.haskell.org/Kind)
* [GHC8.0.2 User's Guide - The Constraint Kind](https://downloads.haskell.org/~ghc/8.0.2/docs/html/users_guide/glasgow_exts.html#the-constraint-kind)
* [constraintsパッケージのリポジトリページ](https://github.com/ekmett/constraints)
* [Ekmett先生のconstraintsパッケージに対するありがたいお言葉](https://www.reddit.com/r/haskell/comments/4vn8og/dataconstraint_dict_as_paramaeter_for_function/)
* [Reflecting values to types and back](https://www.schoolofhaskell.com/user/thoughtpolice/using-reflection)
