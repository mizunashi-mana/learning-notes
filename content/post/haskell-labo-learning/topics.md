---
date: 2017-06-15T03:16:44+09:00
categories:
- haskell-labo-learning
title: Haskell研究室内勉強会
tags:
- haskell
---

Haskell勉強会 in 研究室

| 名前 | 情報 |
| --- | --- |
| 期間 | 2017/06 ~ |
| 場所 | 研究室 |

<!--more-->

## 概要

* Haskellに関する話題
    - 発表したい人は、いつでも言ってくれ〜
    - 1テーマ時間無制限

## Current

* [Open Data Types](/post/haskell-labo-learning/intro-open-data-types/)

## ネタ

* freer monad(free functor + free monad)とOpen Union(関手の有限合成)
    - extensible-effectsパッケージの概要
* Lazy I/OとIteratee
    - Lazy I/Oはモナド則を壊す話と、Iterateeの紹介
* mono-traversableパッケージの紹介
    - より柔軟なfunctor ~ traversableのクラス
* mmorphパッケージの紹介
    - monadの圏の射による，モナドトランスフォーマの一般化
* lensの使い方
    - lensのオペレータとクラスの紹介
* zipperと微分とlensと
    - zipperの紹介と代数的データ型の微分について
* Control.Arrowの使いどころ
    - ArrowクラスのチュートリアルとControl.Arrowのオペレータの紹介
* doctestの書き方について
    - cabal-doctestのセットアップ方法の紹介
* safe-exceptionsによる例外制御
    - safe-exceptionsが生まれるまでの過程とその使い方
* vectorデータ構造の紹介
    - treeでのvectorの表し方と、short cut fusionについて
* optparse-applicativeの紹介
    - 使い方と内部実装について
* Type-safe coercionの仕組みと、頻出するパターン
    - coercionの仕組みと、頻出する活用パターンの紹介
* WHNF戦略の概要
    - Haskellの評価戦略について
* RoleAnnotations拡張について
    - TypeFamilyの未定義動作と、それを解決する言語拡張について
* Core言語の読み方
    - HaskellのSystem-Foな中間言語、コアについて
* Template Haskell入門
    - Template HaskellでQuasiQuotesを作るまで
* SystemFと多相型推論
    - Hinded-Millerの型推論システムと自由定理など
* recursion-schemesとF代数
    - F代数とfold/unfoldの関係性と、recursion-schemesの紹介
    - 構成的アルゴリズム論
* session typeとパイ計算
    - パイ計算の紹介と、それに型付けを行うsession typeのHaskellでのエンコーディング法
* 正格性解析と最適化
    - 正格性解析による最適化とBangPatterns拡張によるスペースリークの防御法
* Dependent Polynomial FunctorとしてのGADTs
    - GADTsはdependentを付けたpolynomial functor
    - Indexed functorとの関係性みたいなの
* Introduction of Generic Haskell
    - scrap your boilerplateとgenericから始まる，Haskellでのジェネリックプログラミング事情
    - https://www.slideshare.net/konn/metaprogramming-in-haskell
* Hasパターンとそれまでのアプローチ
    - UnliftedIO/MonadIO
    - 乱立するモナドの抽象化
* Profunctor
    - Profunctorと2-category
* Monad
    - Monadは1 -> 2-categoryのlax 2-functorだよ
    - Monadから代数を作れる場合があるよ
* Free Monads for Less
    - Free MonadのCodensity representationによる最適化
    - http://www.janis-voigtlaender.eu/mpc2008-slides.pdf

## Done

* [型クラスについて](/post/haskell-labo-learning/about-type-class/)
* [kan-extensionsパッケージと圏論の基礎](/post/haskell-labo-learning/intro-kan-extensions/)
