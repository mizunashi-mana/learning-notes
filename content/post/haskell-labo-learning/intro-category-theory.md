---
categories:
- haskell-labo-learning
date: 2017-06-22T13:42:36+09:00
tags:
- haskell
- category
title: 圏論の基本事項
---

## 圏論の気持ち

* 圏はグラフに、道に関する演算を足し、制約をつけたもの
    - つまり、圏論は、点と有向道に対しての議論をする分野
* 特に圏論は点(対象)がどういうものかを、道(射)にのみ注目して論じていく分野
* 例えば
    - この対象は、全ての対象から一つだけ射があるもの
    - 二つの対象には、二つの逆向きの射があって、その射を演算するとある射になる
* 圏論の元々の目的は、自然性を表現することだった

<!--more-->

## 圏(category)

### 圏の定義

圏$C$は、次の6つ組$(Obj( C ), Mor( C ), dom, cod, id, \circ)$:

| 表記 | 名前 | 種類 |
| --- | --- | --- |
| $Obj( C )$ | 対象(object) | 族(class) |
| $Mor( C )$ | 射(morphism) | 族(class) |
| $dom$ | 始域(domain) | 写像(map): $Mor( C ) \to Obj( C )$ |
| $cod$ | 終域(codomain) | 写像(map): $Mor( C ) \to Obj( C )$ |
| $id$ | 恒等射(identity) | 写像(map): $Obj( C ) \to Mor( C )$ |
| $\circ$ | 合成(composition) | 部分写像(partial map): $Mor( C ) \times Mor( C ) \rightsquigarrow Mor( C )$ |

で、以下の性質を満たすもの:

* 合成閉: $\forall f, g \in Mor( C )\ldotp (dom(f) = cod(g) \Rightarrow \exists h = f \circ g \in Mor( C )\ldotp dom(h) = dom(g) \land cod(h) = cod(f))$
* 恒等射: $\forall f \in Mor( C )\ldotp f \circ id(dom(f)) = id(cod(f)) \circ f = f$
* 結合律: $\forall f, g, h \in Mor( C )\ldotp (\exists fgh = f \circ g \circ h \Leftrightarrow f \circ (g \circ h) = (f \circ g) \circ h = fgh)$

いくつかの表記法を導入する:

* $f: X \to Y \Leftrightarrow_{def} X = dom(f) \land Y = cod(f)$
* $Hom(X, Y) = Hom_C(X, Y) = C(X, Y) = \\{ f \mid f: X \to Y \in Mor( C ) \\}$
* $id_X = id(X)$

これらの表記法を使って圏の性質を書き直すと:

* 合成閉: $f \in Hom(X, Y) \land g \in Hom(Y, Z) \Rightarrow f \circ g \in Hom(X, Z)$
* 恒等射: $\forall f: X \to Y \in Mor( C )\ldotp f \circ id\_X = id\_Y \circ f = f$
* 結合律: $\forall f: Z \to W, g: Y \to Z, h: X \to Y \in Mor( C )\ldotp f \circ (g \circ h) = (f \circ g) \circ h$

### 圏の例

* 1/2/3圏
* 空圏
* モノイドの圏$(1, M, \cdot)$/Mon
* Set

### 小圏

* $Obj( C )$や$Mor( C )$は基本的にクラス(なので、もちろん$Hom(X, Y)$もクラス)
    - $Obj( C )$及び$Mor( C )$が集合であるような圏を小圏(small category)という
    - $Hom(X,Y)$が集合であるような圏を局所小(locally small)という
    - 小圏$\Rightarrow$局所小(逆は成り立たない) 

### 同型射(isomorphism)

* $f: X \to Y$が同型射: $\exists g: Y \to X\ldotp f \circ g = id\_Y \land g \circ f = id\_X$
    - この時$g$を逆射(inverse)という
    - また、$X$と$Y$は同型(isomorphic)であるという

逆射の一意性の証明

### 終対象(terminal object)

* 通常、1と表記

一意性の証明

## 普遍性(universality)

### 普遍写像性(UMP)

* ある対象$X$と、その対象$X$から別の対象$Y$に向かう射$f: X \to Y$の組において、任意の対象$Z$と射$g: Z \to Y$があった時、必ず$g = f \circ h$となる射$h: Z \to X$が存在して一意になる。このような性質を普遍性(または普遍写像性)という
    - 実際にはもう少し一般化ができる(関手の章で見る)
    - 普遍性によって対象を定義する操作は極限の構成の特殊化である(極限の章で見る)

### 直積(product)

* 対象$X, Y$の直積とは、次の三つ組$(X \times Y, p\_X: X \times Y \to X, p\_Y: X \times Y \to Y)$で、以下の性質を満たすもの
    - $\forall Z \in Obj( C )\ldotp (\exists f\_1: Z \to X, f\_2: Z \to Y \Rightarrow \exists ! f\ldotp f\_1 = p\_X \circ f) \land f\_2 = p\_Y \circ f$

一意性の証明

### ファイバー積(pullback)

### 等化子(equalizer)

## 圏に関する諸概念

### epiとmonic

### 部分対象(subobject)

## (圏論的)双対(categorical dual)

### 双対圏

* 圏$C$の双対圏$C^{op}$:
    - $Obj( C^{op} ) = Obj( C )$
    - $C^{op}$の$dom = C$の$cod$
    - $C^{op}$の$cod = C$の$dom$
    - $C^{op}$の$f \circ g = C$の$g \circ f$

* 実際には双対の概念を濫用して、$C^{op}$と圏同値な圏$D$について、$D$は$C$の双対圏であるという場合がある(圏の同型を参照)

### 始対象(initial object)

* 通常、0と表記

終対象の双対

* 始対象かつ終対象なものを、零対象(zero object/null object)という

### 直和(coproduct)

直積の双対

### 余ファイバー積(pushout)

ファイバー積の双対

### 余等化子(coequalizer)

等化子の双対

## 関手(functor)

### 関手の定義

関手$F$はある圏からある圏への射であり、圏の対象から対象への写像と射から射への写像の組である。この組をいちいち表記することはめんどくさいので、両方$F$という表記を使う場合が多い。圏という代数においての準同型写像のようなもの

圏$C$から圏$D$への関手$F: C \to D$とは、

* $C$の対象$X$を$D$の対象$F(X)$に対応させる
* $C$の射$f: X \to Y$を$D$の射$F(f): F(X) \to F(Y)$に対応させる

もので、以下の性質を満たすもの

* 単位元の保存: $F(id\_X) = id\_{F(X)}$
* 合成の保存: $F(f \circ g) = F(f) \circ F(g)$

### 関手の例

* 恒等関手$1_C: C \to C$
    - $1_C(X) = X$
    - $1_C(f) = f$
* 自己関手$F: C \to C$
* 対角関手$\Delta: C \to C \times C$
    - $\Delta(X) = (X, X)$
    - $\Delta(f) = (f, f)$

## 関手に関する諸概念

### 普遍射(universal morphism)

* 圏$C, D$において、関手$U: D \to C$と$C$の対象$X$において、$X$から$U$への普遍射とは、$D$の対象$A$と$C$の射$\phi: X \to U(A)$の対$(A, \phi)$であり、以下の性質を満たす:
    - $\forall f: X \to U(Y) \in Mor( C )\ldotp \exists ! g: A \to Y \in Mor( D )\ldotp f = U(g) \circ \phi$

* 普遍射で特徴付けられるような場合、普遍性を持つという

直積の例:

* $U$に、対角関手$\Delta: C \to C \times C$
* $A$に、直積の対象$X \times Y$
* $\phi$に、直積の射$(p\_1, p\_2)$

とした時、$(X \times Y, (p\_1, p\_2))$は余普遍射

### 図式(diagram)

* Cの図式とは、圏$I$から$C$への関手$D: I \to C$のこと
    - 図式$D$を、$I$で形作られた$C$上の図式("I-shaped diagram in C")などという
    - Iを添字圏(index category/scheme)という

### 充満性(full)と忠実性(faith)

* 圏$C, D$において、関手$F: C \to D$は、$C$の対象$X, Y$において以下のような写像$F_{X, Y}: Hom\_C(X, Y) \to Hom\_D(F(X), F(Y))$を誘導する:
    - $F_{X, Y}(f) = F(f)$

この時、

* $F$が忠実とは、任意の対象$X, Y$において$F_{X, Y}$が単射であるということ
* $F$が充満とは、任意の対象$X, Y$において$F_{X, Y}$が全射であるということ

### 本質的全射

## 圏の構成(constructions on categories)

### 部分圏(subcategory)

### 骨格(skeleton)

### 圏の積(product category)

### 射の圏(arrow category)

### スライス圏(slice category)

### コンマ圏(comma category)

$C$、$D\_1$、$D\_2$を圏として、

## 自然変換(natural transformer)

### 自然変換の定義

* 圏$C$、$D$において、関手$F: C \to D$から$G: C \to D$への自然変換$\eta: Obj( C ) \to Mor( D )$は、以下の写像:
    - $\forall X \in Obj( C )\ldotp \eta\_X \in Hom\_D(F(X), G(X))$
    - $\forall f \in Hom\_C(X, Y)\ldotp \eta\_Y \circ F(f) = G(f) \circ \eta\_X$

なお、関手$F$から$G$への自然変換$\eta$を、$\eta: F \Rightarrow G$と表記する。

### 自然同型

関手$F, G: C \to D$において、自然変換$\theta: F \Rightarrow G$における、$C$の各対象$a$に対する$\theta_a$が同型射である時、$\theta$を自然同型という

自然同型$\theta: F \Rightarrow G$が存在する時、$F \cong G$と表す

### 圏同値

### 関手圏

## デカルト閉圏(cartesian closed category)

### 冪対象(exponential object)

* 対象$X$と$Y$の直積を備える圏$C$の冪対象$X^Y$とは以下の性質を満たすもの
    - 対象$X$と$Y$において評価射(evaluation map)$ev: X^Y \times Y \to X$がある
    - 任意の対象$Z$において、$f: Z \to X$となる射がある時、存在して一意となる射$u: Z \to X^Y$があり以下を満たす

        $$f = ev \circ (u \times id)$$

### デカルト閉圏の定義

* ある圏$C$がデカルト閉であるとは、以下の性質を満たすこと
    - $C$において終対象がある
    - $C$の任意の二つの対象の直積もまた$C$の対象である
    - $C$の任意の二つの対象の冪対象もまた$C$の対象である

* デカルト閉である圏をデカルト閉圏と言う

### デカルト閉圏の例

* 圏Setにおいて
    - 単位集合`1`はSetにおける終対象
    - 集合の直積は、各集合の要素のペアの集合であり、Setの対象である
    - 集合の冪対象は、配置集合であり、Setの対象である

各性質の証明

### カリー＝ハワード＝ランベック対応

## F-代数(F-algebra)

### F-代数の定義

圏$C$とその自己関手$F: C \to C$において、$F$-代数とは$C$の対象$A$と$C$の射$\alpha: F(A) \to A$の組$(A, \alpha)$

### F-代数の圏

* F-代数の準同型射
    - ある二つのF-代数$(A, \alpha)$、$(B, \beta)$において、$C$の射$f: A \to B$が、$F(f) \circ \alpha = \beta \circ f$を満たす時、$f$を$(A, \alpha)$から$(B, \beta)$への準同型射という

* F-代数を対象に、F-代数の準同型射を射にし、$C$の合成をそのまま用いた圏を、F-Alg圏という

* F-Alg圏においての始対象を始代数、終対象を終代数という
    - 終代数は、$C$における終対象1と恒等関手$id\_1$の対$(1, id\_1)$のみである

$(1, id\_1)$が終代数であることの証明

### F-始代数の分解

* $\coprod\_i F\_i$-代数における始代数$(T, in : \coprod\_i F\_i(T) \to T)$について
    - $F\_i$-代数$(T, in \circ j\_i : F\_i(T) \to T)$は始代数($j\_i$は直和の余射影)
    - 逆に始代数$(T, in\_i: F\_i(T) \to T)$から、直和のUMPより$\coprod\_i F\_i$-始代数$(T, \coprod\_i in\_i)$が構築できる

### catamorphismとanamorphism

* catamorphism: $F$-$Alg$における始代数から代数への射
* anamorphism: $F$-$Alg^{op}$における余代数から終余代数への射

### Lambekの補題(Lambek's lemma)

* F-始代数$(T, in)$において、射$in: F(T) \to T$は同型射
    - 逆射$in^{-1}$は、$(F(T), F(in))$への準同型射

補題の証明(F-代数$(F(T), F(in))$を、挟んだ二段格子を作る)

* これは、$T \cong F(T)$を表しており、即ち$T$が$F$における不動点であることを示している

## 表現可能関手

### Hom関手

* $Hom(A, -): C \to Set$
    - $Hom(A, -)(X) = Hom(A, X)$
    - $Hom(A, -)(f: X \to Y) = Hom(A, f): Hom(A, X) \to Hom(A, Y); g \mapsto f \circ g$ 

* $Hom(-, B): C \to Set$
    - $Hom(-, B)(X) = Hom(X, B)$
    - $Hom(-, B)(f: X \to Y) = Hom(f, B): Hom(X, B) \to Hom(Y, B); g \mapsto g \circ f$ 

### 関手の表現

## 極限(limit)

### 自由(free)

### 錐(cone)

## 随伴(adjunctions)

### 随伴の定義

圏$C$、$D$において、関手$F: C \to D$、$G: D \to C$を考える。この時、$Hom\_C(c, G d) \cong Hom\_D(F c, d)$が成り立つ時、$F$、$G$を随伴関手といい、$F \dashv G: C \to D$と表記する。この時、$F$を左随伴関手、$G$を右随伴関手という。

### 単位元と余単位元

## カン拡張(kan extension)

## 高次の圏(higher category)

### モノイド圏(monoidal category)

### 豊穣圏(enriched category)

## モナド(monad)

### モナドの定義

### モノイド対象(monoid object)

### モナドは単なる自己関手の圏(のモノイド圏)におけるモノイド対象だよ。何か問題でも？

### モナドは単なる(自明な2-圏)1から2-圏へのlax 2-関手だよ。何か問題でも？

### クレイスリ圏(kleisli category)

## Hask圏

### Hask圏

* Haskellの単相型を対象、単相型の関数を射、`(.)`関数を合成とした圏をHask圏という

* 対象の例: `Int`/`()`/`[Int]`/`Either String Int`
* 射の例: `toUpperCase :: Char -> Char`/`id :: Int -> Int`/`show :: Int -> String`

* 性質の確認
    - 二つの関数を`(.)`で合成したものは確かに関数であるので、合成で閉じている
    - `id :: A -> A`は`A`の恒等射であり、`f . id = \x -> f (id x) = \x -> f x = f = id . f`
    - `(f . g) . h = \y -> (\x -> f (g x)) (h y) = \y -> f (g (h y)) = f . \y -> g (h y) = f . (g . h)`

* 多相型の関数はそれぞれの具象型に対して、別々の関数を提供すると考えられる(`id :: a -> a`: `id :: () -> ()`/`id :: Int -> Int`/etc.)
* パラメータを持つデータ型は、それぞれの具象型に対して、別々の肩を提供すると考えられる(`Maybe a`: `Maybe ()`/`Maybe Int`/etc.)
* 型制約は、多相化の範囲を制限しているだけ(`(+) :: Num a => a -> a -> a`: `(+) :: Int -> Int -> Int`/`(+) :: Double -> Double -> Double`/etc.)

Haskellの`Category`型クラスにおける、`(->)`のインスタンスは、Hask圏のエンコーディング

```haskell
{-| (k, cat, (.)) is a category

law:
* @id . (f :: cat a b) = f = f . id@
* @(f . g) . h = f . (g . h)@
-}
class Category (cat :: k -> k -> *) where
  id :: cat a a
  (.) :: cat a b -> cat b c -> cat a c

instance Category (->) where
  id = Prelude.id -- = \x -> x
  (.) = (Prelude..) -- = \f g x -> f $ g x
```

### Hask圏の自己関手

Haskellの`Functor`型クラスは、$F: Hask \to Hask$のような関手のエンコーディング

```haskell
-- | 'f'は対象'a'を'f a'に写す
class Functor (f :: * -> *) where
    -- | 'fmap'は射'a -> b'を射'f a -> f b'に写す
    fmap :: (a -> b) -> (f a -> f b)
```

### 自然変換と自由定理

* `f :: (Functor f, Functor g) => f a -> g a`は自然変換となる(自由定理)
    - $a \cong Id(a)$より、`forall a. a -> f a`や`forall a. f a -> a`も自然変換
    - `forall a b. a -> b -> a`は`forall a b. a -> ((->) b) a`と同じ。これも自然変換

* 自由定理のもう一つの本質は、多相関数の実装は限られること:
    - `forall a. a -> a`は`\x -> x`のみ
    - `forall a b. a -> b -> a`は`\x _ -> x`のみ
    - `Functor`型クラス則は、どちらか一つを満たせばもう一方も自由定理から満たされる

### Haskと遅延評価

* Haskは正しくは圏ではない
    - counterexample: `id . undefined = \x -> id (undefined x) = \x -> id undefined = \x -> undefined`
    - ``id . undefined `seq` () = ()``だが、``undefined `seq` () = undefined``

* 積のUMPの破壊

    ```haskell
    data A
    data B
    data C

    f :: C -> A
    f _ = undefined

    g :: C -> B
    g _ = undefined

    h :: C -> (A, B)
    h _ = (undefined, undefined)

    h' :: C -> (A, B)
    h' _ = undefined

    diffP :: (A, B) -> ()
    diffP (_, _) = ()
    diffP _      = undefined
    ```

* 慣例では、Haskellのボトムを除いたサブセット(つまり、計算が必ず終了し、未定義にもならない)を考えるようにする
    - または正格計算を行うようにする

## References

### Main

* 壱大整域 - 圏論: http://alg-d.com/math/kan_extension/
* What I Wish I Knew When Learning Haskell - Categories: http://dev.stephendiehl.com/hask/#categories

### Specific

* Category Theory and Haskell:
    - Part 1: http://www.alpheccar.org/content/74.html 
    - Part 2: http://www.alpheccar.org/content/76.html
    - 3 - Algebra and Monads: http://www.alpheccar.org/content/77.html
* Haskell Wiki - Hask: https://wiki.haskell.org/Hask
* F-Algebra survey: https://www.tu-braunschweig.de/Medien-DB/iti/survey_full.pdf
* The free theorem for fmap: https://www.schoolofhaskell.com/user/edwardk/snippets/fmap
