---
title: "kan-extensionsパッケージの紹介"
categories:
- haskell-labo-learning
date: 2017-08-09T22:36:50+09:00
tags:
- haskell
---

レジュメ。

## 構成

1. Haskellの様々な関手
    1. FunctorとApplicative Functor
    1. BifunctorとProfunctor
    1. Traversable Functorクラスとその双対
    1. Goal: Haskellの関手表現の理解
1. 随伴
    1. distributiveパッケージとRepresentable Functor
    1. 随伴について
    1. Goal: adjunctionsパッケージの理解
1. カン拡張
    1. 米田の補題について
    1. カン拡張について
    1. Goal: kan-extensionsパッケージの理解

## 様々な関手

### 関手(functor): Functorクラス

* 圏$C$、$D$において、以下を満たす写像$F$を関手という:
    - $\forall a \in Obj( C )\ldotp F(a) \in Obj( D )$
    - $\forall f \in Hom\_C(a, b)\ldotp F(f) \in Hom\_D(F(a), F(b))$
    - $F(id\_a) = id\_{F(a)}$
    - $F(f . g) = F(f) . F(g)$

* HaskellではHask自己関手を`base`パッケージの`Data.Functor.Functor`型クラスで表現:

    ```haskell
    --| Endofunctor of Hask
    --
    -- * obj map: a in Obj(Hask) -> f a in Obj(Hask)
    -- * mor map: f in Hom(a, b) -> fmap f in Hom(f a, f b)
    --
    -- * fmap id = id
    -- * fmap (f . g) = fmap f . fmap g
    --
    class Functor (f :: * -> *) where
        fmap :: (a -> b) -> (f a -> f b)
    ```

### 反変関手(contravariant functor): Contravariantクラス

* 圏$C$、$D$において、関手$F: C^{op} \to D$を反変関手という

* HaskellではHask自己反変関手を`contravariant`パッケージの`Data.Functor.Contravariant.Contravariant`型クラスで表現:

    ```haskell
    --| Endo contravariant functor of Hask
    --
    -- * obj map: a in Obj(Hask) -> f a in Obj(Hask)
    -- * mor map: f in Hom(a, b) -> fmap f in Hom(f b, f a)
    --
    -- * contramap id = id
    -- * contramap (f . g) = contramap g . contramap f
    --
    class Contravariant (f :: * -> *) where
        contramap :: (a -> b) -> (f b -> f a)
    ```

### 双関手(bifunctor): Bifunctorクラス

* 圏$C\_1$、$C\_2$、$D$において、関手$F: C\_1 \times C\_2 \to D$を双関手という

* HaskellではHask双関手を`base`パッケージの`Data.Bifunctor.Bifunctor`型クラスで表現:

    ```haskell
    --| Bifunctor of Hask
    --
    -- * obj map: (a, b) in Obj(Hask \times Hask) -> p a b in Obj(Hask)
    -- * mor map: (f, g) in Hom((a, c), (b, d)) -> bimap f g in Hom(p a c, p b d)
    --
    -- * bimap id id = id
    -- * bimap (f . g) (h . i) = bimap f h . bimap g i
    --
    class Bifunctor (p :: * -> * -> *) where
        bimap :: (a -> b) -> (c -> d) -> (p a c -> p b d)
    ```

### Profunctor: Profunctorクラス

* 圏$D$、$C$において、反変関手$F: D^{op} \to Set^{C}$をprofunctorという

* Haskellでは、Hask双関手として`profunctors`パッケージの`Data.Profunctor.Profunctor`型クラスで表現:

    ```haskell
    --| Profunctor of Hask
    --
    -- * obj map: (a, b) in Obj(Hask^{op} \times Hask) -> p a b in Obj(Hask)
    -- * mor map: (f, g) in Hom((a, d), (b, c)) -> dimap f g in Hom(p a d, p b c)
    --
    -- * dimap id id = id
    -- * dimap (f . g) (h . i) = dimap f i . fmap g h
    --
    class Profunctor (p :: * -> * -> *) where
        dimap :: (a -> b) -> (c -> d) -> (p a d -> p b c)
    ```

### Traversable functor: Traversableクラス

* Haskellの表現:

    ```haskell
    --| Traversable functor of Hask
    --
    -- let `t` is any applicative transformers:
    -- * t :: (Applicative f, Applicative g) => f a -> g a
    --
    -- * t . sequenceA = sequenceA . fmap t
    --
    class (Functor t, Foldable t) => Traversable t where
        sequenceA :: t (f a) -> f (t a)
    ```

### アプリカティブ関手(applicative functor): Applicativeクラス

* Applicative/Monadの圏論的意味については、今度詳しくやる
    - applicative functors = strong lax monoidal functors
    - applicative = strong * monoidal * lax
    - monadもlax functorの一種

* HaskのApplicative endofunctorを`base`パッケージの`Control.Applicative.Applicative`型クラスで表現

    ```haskell
    --| Applicative endofunctor of Hask
    --
    -- * (<*>) . pure = fmap <-> (pure f <*>) = fmap f
    --
    -- * identity: fmap id = id
    -- * composition: fmap (.) u <*> v <*> w = u <*> (v <*> w)
    -- * homomorphism: fmap f . pure = pure . f
    -- * interchange: fmap ($ y) u = u <*> pure y
    --
    class Functor f => Applicative (f :: * -> *) where
        pure :: a -> f a
        (<*>) :: f (a -> b) -> (f a -> f b)
    ```

## 随伴関手

圏$C$と$D$において、関手$F: C \to D$と$G: D \to C$が随伴であるとは、

* $C(F(-), -) \cong D(-, G(-))$であること


## カン拡張

## Links

* [Moore for Less](https://www.schoolofhaskell.com/user/edwardk/moore/for-less)
