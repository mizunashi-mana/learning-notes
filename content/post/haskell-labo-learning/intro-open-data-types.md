---
title: "Intro Open Data Types"
date: 2018-02-26T00:47:04+09:00
categories:
- haskell-labo-learning
tags:
- haskell
---

# Open Data Types

## Expression ProblemとOpen性

プログラムにおいての2つの拡張性:

* データ型の拡張性
* 関数(データ型に対する処理)の拡張性

そもそものExpression Problem [Wad98] :

* 2つ同時に拡張性を習得できるか

| 拡張性 \ 言語 | 関数型 | オブジェクト指向 |
| --- | --- | --- |
| データ型 | 固定 | サブクラスで幾らでも後から追加できる |
| 関数    | 幾らでも関数を追加できる | 固定 |

Expression Problem [ZO05] :

Wadlerのものに加えて，次のものを追加

* 既存のコードに対して，再コンパイルの必要がない
* 独立して拡張機能を作成可能

解決案:

* マルチメソッド(Clojure/etc.)
* オープンクラス(Ruby/etc.)
* 多相バリアント(OCaml/etc.)
* Data types a la carte(Coproducts of functors)
* Tagless final

Expression Problemは，データ型のSumに関する拡張性に着目した問題 -> より一般的な拡張性の問題(Open性)

open for sum:

```haskell
open data Expr
Val :: Int -> Expr
Add :: Expr -> Expr -> Expr

open eval :: Expr -> Int
eval (Val i)     = i
eval (Add e1 e2) = eval e1 + eval e2
```

open for recursion:

```haskell
openrec funcBase :: Int -> Int
funcBase n = if n == 0 then 0 else n + self (n - 1)
```

再帰データ型や再帰関数が絡むと，Expression Problemはかなり厄介な問題となる -> この辺の問題で結構色々昔から議論があった

## polynomial functorとrecursion schemes

* 代数的データ型 = A (least) fixed point of polynomial functor

Parts of polynomial functors of Sets:

```haskell
{-# LANGUAGE TypeOperators #-}

-- 'K'onstant
newtype K c a = K c

-- Identity
newtype I a = I a

-- Sum
data (f :+: g) a = InL (f a) | InR (g a)

-- Product
data (f :*: g) a = Pair (f a) (g a)

-- Unit
type U = K ()
```

Examples of fixed points of polynomial functors:

```haskell
{-# LANGUAGE PatternSynonyms #-}

-- | Fixed point
-- 
-- Recursive type synonyms are not allowed in Haskell:
-- `type Some = SomeF Some -- not allowed`
--
-- But, recursive data types are allowed in Haskell:
--
newtype Fix f = Fix { unFix :: f (Fix f) }


-- data Identity a = Identity a
type IdentityF a = K a
type Identity a = Fix (IdentityF a)

pattern Identity :: a -> Identity a
pattern Identity x = Fix (K x)


-- data Maybe a = Nothing | Just a
type MaybeF a = U :+: K a
type Maybe a = Fix (MaybeF a)

pattern Nothing :: Maybe a
pattern Nothing = Fix (InL (K ()))

pattern Just :: a -> Maybe a
pattern Just x = Fix (InR (K x))


-- data Bool = True | False
type BoolF = K () :+: K ()
type Bool = Fix BoolF

pattern True :: Bool
pattern True = Fix (InL (K ()))

pattern False :: Bool
pattern False = Fix (InR (K ()))


-- data List a = Nil | Cons a (List a)
type ListF a = U :+: (K a :*: I)
type List a = Fix (ListF a)

pattern Nil :: List a
pattern Nil = Fix (InL (K ()))

pattern Cons :: a -> List a -> List a
pattern Cons x xs = Fix (InR (Pair (K x) (I xs)))


-- data BinTree a = Leaf | BinNode a (BinTree a) (BinTree a)
type BinTreeF a = U :+: (K a :*: I :*: I)
type BinTree a = Fix (BinTreeF a)

pattern Leaf :: BinTree a
pattern Leaf = Fix (InL (K ()))

pattern BinNode :: a -> BinTree a -> BinTree a -> BinTree a
pattern BinNode x t1 t2 = Fix (InR (Pair (Pair (K x) (I t1)) (I t2)))
```

A zoo of morphisms:

endofunctor$F: C \to C$に対して，

* F-algebraの始対象となるような$\mathit{Fix}_F \in C$を`F`のleast fixed point
* F-coalgebraの終対象となるような$\mathit{Cofix}_F \in C$を`F`のgreatest fixed point

とそれぞれ呼ぶ．Lambekの補題より，$\mathit{Fix}_F \cong F(\mathit{Fix}_F)$，$\mathit{Cofix}_F \cong F(\mathit{Cofix}_F)$が成り立つ．

| F-algebra | F-coalgebra |
| --- | --- |
| $F A \to A$ | $A \to F A$ |
| algebra | system |
| initial | final |
| least fixed point | greatest fixed point |

* catamorphism: basic recursion scheme

    $f: F(X) \to X$に対して，$(|f|): \mathit{Fix}_F \to X$がinitialityより存在する．この$(|f|)$を**catamorphism**と言う．

    なお，$\mathit{Fix}_F$の同型射$\mathit{out}: \mathit{Fix}_F \to F(\mathit{Fix}_F)$を使って，$(|f|) = \mathit{out}; F((|f|)); f$から，不動点コンビネータ$\mathit{fix}$を使って

    $$(|f|) = \mathit{fix}_{\mathit{out}; F(-); f}$$

    というように，構築ができる．

* anamorphism: basic corecursion scheme (dual catamorphism)

    $g: X \to F(X)$に対して，$[|g|]: X \to \mathit{Cofix}_F$がterminalityより存在する．この$[|g|]$を**anamorphism**と言う．

* mutumorphism: mutually (co)recursion scheme

    $f: F(X) \to X$，$p: X \to Y$に対して，$\mathit{mutu}(f, p): \mathit{Fix}_F \to Y = (|f|); p$を**mutumorphism**と言う．

    ついでに，anaを拡張したものも同じくmutuと呼ばれる(こちらはまずinjectionを行う)．

    そのインスタンス:

    - zygomorphism: useful mutual recursion scheme

        $p: F(X) \to X$，$q: F(X \times Y) \to Y$に対して，$(F(\pi_1); p) \times q: F(X \times Y) \to X \times Y$みたいなものを考える．この時，$\mathit{zygo}(p, q) = \mathit{mutu}((F(\pi_1); p) \times q, q): \mathit{Fix}_F \to Y$を**zygomorphism**と言う．

    - paramorphism: folding recursion scheme

        $p: F(\mathit{Fix}_F \times X) \to X$に対して，$\mathit{in}: F(\mathit{Fix}_F) \to \mathit{Fix}_F$を持って来た時，$\mathit{para}(p) = \mathit{zygo}(\mathit{in}, p): \mathit{Fix}_F \to X$を**paramophism**と言う．

    - apomorphism: dual of paramorphism

* recursion scheme for comonad: generalized catamorphism from comonads

    インスタンス:

    - histomorphism

## Data types a la carte

* データ型の拡張 -> Coproduct of functors(polynomial functorsでデータ型を書き，最後にFix)
* パターンマッチの拡張 -> Coproduct of algebras(型クラスにalgebrasを登録し，foldは型制約のままでalgebraをfix)
* automatic injection -> 型クラスでsubtyping関係を作り，injectionを作る(right assocなものしかうまく扱えない)

better automatic injection:

```haskell
{-
-- need right assoc property
data (f :+: g) a = InL (f a) | InR (g a)
  deriving Functor
-}
class Summable (fs :: [* -> *]) where
  data Summed fs :: * -> *

instance Summable '[] where
  data Summed '[] a = SummedNil Void
    deriving Functor

instance Summable (f ': fs) where
  data Summed (f ': fs) a
    = Here (f a)
    | Elsewhere (Summed fs a)

deriving instance (Functor f, Functor (Summed fs)) => Functor (Summed (f ': fs))


class Injectable (f :: * -> *) (fs :: [* -> *]) where
  inj :: f a -> Summed fs a

instance Injectable f (f ': fs) where
  inj = Here

instance Injectable f fs => Injectable f (g ': fs) where
  inj = Elsewhere . inj
```

(その他にも，そもそも型レベルのMemberを実装すればいいという話もある -> open union)

# Finally Tagless Approach

## church encodingとADT

* church encoding -> (型無し)ラムダ計算でのADTのエンコーディングの仕方(他: scott encoding)

* Boehm-Berarducci encoding -> 代数的データ型と同等のエンコーディングとパターンマッチを提供

```haskell
data Nat = Succ Nat | Zero


data ChurchNat = ChurchNat (forall r. (r -> r) -> r -> r)

succ :: ChurchNat -> ChurchNat
succ (ChurchNat n) = ChurchNat (\s z -> s (n s z))

zero :: ChurchNat
zero = ChurchNat (\s z -> z)
```

```haskell
-- data U a = U
data U a = U (forall r. r -> r)
instance Functor U where
  fmap _ (U x) = U x

-- data K c a = K c
data K c a = K (forall r. (c -> r) -> r)
instance Functor (K c) where
  fmap _ (K x) = K x

-- data (f :*: g) a = Pair (f a) (g a)
data (f :*: g) a = P (forall r. (f a -> g a -> r) -> r)
instance (Functor f, Functor g) => Functor (f :*: g) where
  fmap t (P x) = P (\p -> x (\fa ga -> p (fmap t fa) (fmap t ga)))

-- data (f :+: g) a = InL (f a) | InR (g a)
data (f :+: g) a = E (forall r. (f a -> r) -> (g a -> r) -> r)
instance (Functor f, Functor g) => Functor (f :+: g) where
  fmap t (E x) = E (\inl inr -> x (\fa -> inl (fmap t fa)) (\ga -> inr (fmap t ga)))
```

## higher order abstract syntax(HOAS)

* lambdaをADTで表現する(本来de brujin index化などが必要だが，これをHaskellのラムダで表現)

* tagless encoding -> GADTs

* ラムダのままでは扱いづらいことも(pretty print/equality) -> phoas

* PHOAS(Parametric HOAS): HOASのa fixed point of polynomial functor化
    - polynomial functorなら幾つか工夫が可能

## Finally Tagless

* initial encoding <-> final encoding

* final encodingをtype class化 -> extensibleなPHOASになる

* initial approach: 再帰的にパターンマッチによってdeconstructしながらtraverseする

    ```haskell
    data IniTerm
      = Val Int
      | Add IniTerm IniTerm

    term :: IniTerm
    term = Add (Val 1) (Add (Val 2) (Val 1))
    ```

* final approach: 基本となるコンビネータを組み合わせて式を作る

    ```haskell
    data FinTerm r = FinTerm
      { val :: Int -> r
      , add :: r -> r -> r
      }

    term :: FinTerm r -> r
    term FinTerm{..} = add (val 1) (add (val 2) (val 1))
    ```

    Type Class Version:

    ```haskell
    class FinTerm r where
      val :: Int -> r
      add :: r -> r -> r

    term :: FinTerm r => r
    term = add (val 1) (add (val 2) (val 1))
    ```

Final <-> Initial

```haskell
-- Final -> Initial
instance FinTerm IniTerm where
  val = Val
  add = Add

-- Initial -> Fin
toFinal :: FinTerm r => IniTerm -> r
toFinal (Val i) = val i
toFinal (Add t1 t2) = add (toFinal t1) (toFinal t2)
```

## References

* [Wad98]: Wadler, P (1998). _The Expression Problem_. In Email of Discussion on the Java Genericity mailing list. http://homepages.inf.ed.ac.uk/wadler/papers/expression/expression.txt
* [ZO05]: Zenger, M., & Odersky, M. (2005). _Independently Extensible Solutions to the Expression Problem_. Fool. https://doi.org/10.1.1.107.4449
* [LH06]: Löh, A., & Hinze, R. (2006). _Open data types and open functions_. In Proceedings of the 8th ACM SIGPLAN symposium on Principles and practice of declarative programming - PPDP ’06 (p. 133). New York, New York, USA: ACM Press. https://doi.org/10.1145/1140335.1140352
* [HWG13]: Hinze, R., Wu, N., & Gibbons, J. (2013). _Unifying structured recursion schemes_. In Proceedings of the 18th ACM SIGPLAN international conference on Functional programming - ICFP ’13 (p. 209). New York, New York, USA: ACM Press. https://doi.org/10.1145/2500365.2500578
* [MC08]: McBride, C. (2008). _Clowns to the left of me, jokers to the right (pearl)_. ACM SIGPLAN Notices, 43(1), 287. https://doi.org/10.1145/1328897.1328474
* [UVP01]: Uustalu, T., Vene, V., & Pardo, A. (2001). _Recursion schemes from comonads_. Nordic J. of Computing, 8(3), 366–390. Retrieved from http://www.cs.helsinki.fi/njc/References/uustaluvp2001:366.html%5Cnhttp://www.fing.edu.uy/~pardo/papers/njc01.ps.gz%5Cnhttp://portal.acm.org/citation.cfm?id=766523
* [BBIso]: Beyond Church encoding: Boehm-Berarducci isomorphism of algebraic data types and polymorphic lambda-terms, http://okmij.org/ftp/tagless-final/course/Boehm-Berarducci.html
